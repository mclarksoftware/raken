# Implemented Features
- Rest Endpoint accepts POST of email content to send
- Endpoint accepts an optional query param `enrich` to trigger append of weather details
- Email content validates and makes call to SendGrid
- Email is logged instead of sent when filter config is true and one or more recipients exist for external domains

# Remaining Tasks
- Necessary tests have been stubbed out, but remain to be completed
- Call to SendGrid has been completed, but was not tested fully as the provided API key is marked as invalid or expired.

# Configuration
- Configuration for running the app can be completed in src/main/java/org/raken/demo/configuration/HardcodedConfigurationSource.java