package org.raken.demo.email;

import java.util.Date;

public class EmailMessage {
    public String to;
    public String cc;
    public String bcc;
    public String subject;
    public String body;
    public Date sentDate;
}
