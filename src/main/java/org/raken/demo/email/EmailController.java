package org.raken.demo.email;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.raken.demo.common.ErrorResponse;
import org.raken.demo.weather.WeatherService;

@RestController
public class EmailController {
    
    private Log log = LogFactory.getLog(EmailController.class);

    @Autowired
    private EmailService emailService;

    @Autowired
    private WeatherService weatherService;
    
    @PostMapping(value="/email", consumes="application/json", produces="application/json")
    public Object index(@RequestBody EmailMessage email, @RequestParam(required=false) boolean enrich) {
        try {
            if (enrich == true) {
                String weather = weatherService.getCurrentWeather();
                if (weather != null) {
                    email.body += "\n\n" + weather;
                }
            }

            emailService.sendEmail(email);
            return email;
        } catch (Exception ex) {
            log.error("Exception occured sending email.", ex);
            return new ErrorResponse(ex.getMessage());
        }        
    }
}