package org.raken.demo.email;

import org.raken.demo.common.ServiceValidationException;
import org.raken.demo.configuration.ConfigurationSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;

@Component
public class EmailService {
    private static final String INTERNAL_RECIPIENT = "@rakenapp.com";
    
    private Log log = LogFactory.getLog(EmailService.class);

    @Autowired
    private ConfigurationSource configurationSource;

    public EmailMessage sendEmail(EmailMessage email) throws Exception {
        validateMessage(email);

        if (shouldLogMessage(email)) {
            logMessage(email);
        } else {
            submitMessage(email);
        }

        email.sentDate = new Date();
        return email;
    }

    protected void validateMessage(EmailMessage email) throws ServiceValidationException {
        if (email.subject == null || email.subject.trim().equals("")) {
            throw new ServiceValidationException("Email Subject is required.");
        }
        if (email.body == null || email.body.trim().equals("")) {
            throw new ServiceValidationException("Email Body is required.");
        }
        if (email.to == null || email.to.trim().equals("")) {
            throw new ServiceValidationException("Email requires at least one TO recipient.");
        }
        validateRecipients(email.to, "TO");
        validateRecipients(email.cc, "CC");
        validateRecipients(email.bcc, "BCC");
    }

    protected void validateRecipients(String recipientList, String type) throws ServiceValidationException {
        if (recipientList == null || recipientList.trim().equals("")) {
            return;
        }
        Pattern addressValidator = Pattern.compile("^(.+)@(.+)\\.(.+)$");
        String[] recipients = recipientList.split(";");
        for (int i=0; i<recipients.length; i++) {
            if (!addressValidator.matcher(recipients[i]).matches()) {
                throw new ServiceValidationException("Invalid recipient address '" + recipients[i] + "' in the " + type + " list");
            }
        }
    }

    /*
    Point of clarification: 
    Under normal circumstances I would seek clarification from my project lead and/or customer/business analyst.
    Specification was unclear if the filtering should just remove the external recipients and
    continue sending to the internls.  The decision was made for this sample application
    to filter the entire message when containing external recipients.
    */
    protected boolean shouldLogMessage(EmailMessage email) {
        if ("true".equalsIgnoreCase(configurationSource.getConfiguration("EMAIL_FILTER"))) {
            return hasExternalRecipients(email.to) ||
                   hasExternalRecipients(email.cc) ||
                   hasExternalRecipients(email.bcc);
        }
        return false;
    }

    protected boolean hasExternalRecipients(String recipientList) {
        if (recipientList == null || recipientList.trim().equals("")) {
            return false;
        }
        String[] recipients = recipientList.split(";");
        for (int i=0; i<recipients.length; i++) {
            if (!recipients[i].endsWith(INTERNAL_RECIPIENT)) {
                return true;
            }
        }
        return false;
    }

    protected void logMessage(EmailMessage email) {
        StringBuilder message = new StringBuilder();
        message.append("To: " + email.to + "\n");
        if (email.cc != null ) { 
            message.append("CC: " + email.to + "\n"); 
        }
        if (email.bcc != null) { 
            message.append("BCC: " + email.to + "\n"); 
        }
        message.append("Subject: " + email.subject + "\n");
        message.append("Body: \n");
        message.append(email.body);

        log.info("External-recipient filtering active, message logged and not sent:\n" + message.toString());
    }

    protected void submitMessage(EmailMessage email) throws IOException {
        String emailApiKey = configurationSource.getConfiguration("SENDGRID_KEY");
        String emailSender = configurationSource.getConfiguration("EMAIL_SENDER");

        Mail mail = new Mail();
        mail.setFrom(new Email(emailSender));
        mail.setSubject(email.subject);
        mail.addContent(new Content("text/plain", email.body));
        
        Personalization recipients = new Personalization();
        List<Email> emails = convertRecipients(email.to);
        for (int i=0; i<emails.size(); i++) {
            recipients.addTo(emails.get(i));
        }

        emails = convertRecipients(email.cc);
        for (int i=0; i<emails.size(); i++) {
            recipients.addCc(emails.get(i));
        }
        
        emails = convertRecipients(email.bcc);
        for (int i=0; i<emails.size(); i++) {
            recipients.addBcc(emails.get(i));
        }
        
        mail.addPersonalization(recipients);
        log.info(emailApiKey);
        SendGrid sg = new SendGrid(emailApiKey);
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());

            if ("true".equalsIgnoreCase(configurationSource.getConfiguration("EMAIL_ACTIVE"))) {
                Response response = sg.api(request);

                if (response.getStatusCode() != 200) {
                    log.error("Error calling SendGrid mail endpoint.  Response-Code: " + response.getStatusCode() + "\n" + response.getBody());
                    throw new IOException("Error occured calling SendGrid mail endpoint.");
                }
            }
        } catch (IOException ex) {
            throw ex;
        }
    }

    protected List<Email> convertRecipients(String recipientList) {
        List<Email> emails = new ArrayList<Email>();
        if (recipientList != null && !recipientList.trim().equals("")) {
            String[] recipients = recipientList.split(";");
            for (int i=0; i<recipients.length; i++) {
                emails.add(new Email(recipients[i]));
            }
        }
        return emails;
    }
}