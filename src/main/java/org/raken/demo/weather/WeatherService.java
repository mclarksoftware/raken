package org.raken.demo.weather;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.http.client.methods.HttpGet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.raken.demo.configuration.ConfigurationSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WeatherService {

    private Log log = LogFactory.getLog(WeatherService.class);

    @Autowired
    private ConfigurationSource configurationSource;

    public String getCurrentWeather() throws Exception {
        Map results = getCurrentConditions();
        if (results == null) {
            return null;
        }

        Map current = (Map) ((List) results.get("dataseries")).get(0);
        
        int temp = (int) current.get("temp2m");
        int windS = (int) ((Map)current.get("wind10m")).get("speed");
        String windD = (String) ((Map)current.get("wind10m")).get("direction");
        String precip = (String) current.get("prec_type");

        StringBuilder weatherBlock = new StringBuilder();
        weatherBlock.append("Current weather conditions for Carlsbad, CA:\n");
        weatherBlock.append("Temperature: " + temp + "F, ");
        weatherBlock.append("Winds " + windD + " @ " + windS + "MPH ");
        weatherBlock.append("Precipitation: " + precip);

        return weatherBlock.toString();
    }

    protected Map getCurrentConditions() throws Exception {
        String endpoint = configurationSource.getConfiguration("WEATHER_ENDPOINT");

        CloseableHttpClient httpClient = HttpClients.createDefault();
        try {
            HttpGet get = new HttpGet(endpoint);
            ResponseHandler<Map> responseHandler = new ResponseHandler<Map>() {
                @Override
                public Map handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                    if (response.getStatusLine().getStatusCode() == 200) {
                        HttpEntity entity = response.getEntity();
                        if (entity == null) {
                            return null;
                        } else {
                            ObjectMapper mapper = new ObjectMapper();
                            return mapper.readValue(EntityUtils.toString(entity), Map.class);
                        }
                    } else {
                        log.error("Error calling for current weather. Status: " + response.getStatusLine().getStatusCode());
                        throw new IOException("Exception calling for current weather.");
                    }
                }
            };

            Map results = httpClient.execute(get, responseHandler);
            return results;
        } finally {
            httpClient.close();
        }
    }    
}
