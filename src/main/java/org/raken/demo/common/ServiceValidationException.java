package org.raken.demo.common;

public class ServiceValidationException extends Exception {
    public ServiceValidationException(String message) {
        super(message);
    }
}
