package org.raken.demo.configuration;

import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

@Component
public class HardcodedConfigurationSource implements ConfigurationSource {

    /*
    HardcodedConfigurationSource supplies configuration based on a hardcoded list
    of key-value pairs for the purpose of this demo application.  A production-ready
    implementation of the ConfigurationSource interface would make use of database,
    file-store, and other secure locations for keys and other configuration.
    */

    private HashMap<String, String> configurations = new HashMap<String, String>();

    @PostConstruct
    public void init() {
        configurations.put("SENDGRID_KEY", "<supply after project checkout>");
        configurations.put("EMAIL_SENDER", "<supply sender email>");
        configurations.put("EMAIL_FILTER", "true");
        configurations.put("EMAIL_ACTIVE", "false");
        configurations.put("WEATHER_ENDPOINT", "https://www.7timer.info/bin/astro.php?lon=-117.35&lat=33.16&ac=0&unit=british&output=json&tzshift=0");
    }

    public String getConfiguration(String name) {
        return configurations.get(name);
    }
}