package org.raken.demo.configuration;

public interface ConfigurationSource {
    public String getConfiguration(String name);
}