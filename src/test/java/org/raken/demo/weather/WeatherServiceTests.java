package org.raken.demo.weather;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class WeatherServiceTests {
    
    @Autowired
    WeatherService weatherService;
    
    @Test
    public void fetchesCurrentConditionsMap() {

    }

    @Test
    public void returnsNullForEmptyResults() {

    }

    @Test
    public void throwsExceptionForFailedCall() {

    }

    @Test
    public void buildsWeatherBlockFromMapResults() {
        
    }
}
