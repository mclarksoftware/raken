package org.raken.demo.email;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EmailServiceTests {

    @Autowired
    EmailService emailService;
    
    @Test
    void validatesEmailSubject() { }

    @Test
    void validatesEmailBody() { }
    
    @Test
    void validatesEmailToRequired(){ }

    @Test
    void validatesRecipientLists(){ }

    @Test
    void splitsRecipientListAndValidatesIndividualAddresses() { }

    @Test
    void determinesIfMessageShouldBeLogged() { }

    @Test
    void buildsEmailIntoTextBlockAndLogs() { }

    @Test
    void buildsEmailMessageIntoSendGridMailAndSends() { }

}
