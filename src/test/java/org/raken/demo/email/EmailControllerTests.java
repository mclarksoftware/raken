package org.raken.demo.email;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EmailControllerTests {
    
    @Test
    void callsEmailServiceToSend() {

    }

    @Test
    void callsWeatherAndAppendsOnEnrich() {

    }

    @Test
    void skipsWeatherAppendWhenResultIsNull() {

    }

    @Test
    void returnsEmailAfterSend() {

    }

    @Test
    void returnsErrorResponseOnException() {
        
    }
}
